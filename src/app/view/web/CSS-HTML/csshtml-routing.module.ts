import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { BotonesComponent } from './botones/botones.component';
import { CarruselInfinitoComponent } from './carrusel-infinito/carrusel-infinito.component';
import { HtmlcssestructuraComponent } from 'src/app/layouts/web/htmlcssestructura/htmlcssestructura.component';



const routes: Routes = [
  {path:'', component: HtmlcssestructuraComponent,
    children:[
      { path: 'botones', component: BotonesComponent },
      { path: 'carrusel', component: CarruselInfinitoComponent },
      { path: '**', pathMatch:'full', redirectTo:'botones'}
    ]
}
 

]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class csshtmlRoutingModule {}
