import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BotonesComponent } from './botones/botones.component';
import { CarruselInfinitoComponent } from './carrusel-infinito/carrusel-infinito.component';
import { csshtmlRoutingModule } from './csshtml-routing.module';
import { HtmlcssestructuraComponent } from 'src/app/layouts/web/htmlcssestructura/htmlcssestructura.component';
import { SharedWebModule } from 'src/app/shared/web/sharedweb.module';
import { CoreModule } from 'src/app/core/core.module';
import { DemoMaterialModule } from 'src/app/material-module';



@NgModule({
  declarations: [
    BotonesComponent,
    CarruselInfinitoComponent,
    HtmlcssestructuraComponent,
    
  ],
  imports: [
    CommonModule,
    csshtmlRoutingModule,
    SharedWebModule,
    CoreModule,
    DemoMaterialModule
  ]
})
export class CsshtmlModule { }
