import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedWebModule } from 'src/app/shared/web/sharedweb.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedWebModule
  ]
})
export class AngularmaterialModule { }
