import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingComponent } from './components/loading/loading.component';
// spinner
import { NgxSpinnerModule } from "ngx-spinner";


@NgModule({
  declarations: [
    LoadingComponent
  ],
  imports: [
    CommonModule,
    // spinner
    NgxSpinnerModule
  ],
  exports: [
    LoadingComponent
  ]
})
export class CoreModule { }
