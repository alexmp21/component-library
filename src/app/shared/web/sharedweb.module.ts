import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarloginComponent } from './navbarlogin/navbarlogin.component';

// iconos
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { NavbarprincipalwebComponent } from './navbarprincipalweb/navbarprincipalweb.component';
import { AuthModule } from 'src/app/auth/web/auth.module';
import { appRoutingModule } from 'src/app/app-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { RouterModule } from '@angular/router';
import { SidebarprincipalwebComponent } from './sidebarprincipalweb/sidebarprincipalweb.component';
import { DemoMaterialModule } from 'src/app/material-module';


// componentes


@NgModule({
  declarations: [
    NavbarloginComponent,
    NavbarprincipalwebComponent,
    SidebarprincipalwebComponent,
  
  ],
  imports: [
    CommonModule,
    NgxSpinnerModule,
    // iconos
    HttpClientModule, AngularSvgIconModule.forRoot(),
    // Para las rutas 
    RouterModule,
   DemoMaterialModule
 
  ],
  exports:[
    NavbarloginComponent,
    NavbarprincipalwebComponent,
    SidebarprincipalwebComponent
  ]
})
export class SharedWebModule { }
