import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebarprincipalweb',
  templateUrl: './sidebarprincipalweb.component.html',
  styleUrls: ['./sidebarprincipalweb.component.css']
})
export class SidebarprincipalwebComponent implements OnInit {
  classList: any;
  nextElementSibling: any;
  
 
  constructor() { }

  ngOnInit(): void {
    const accordionBtns = document.querySelectorAll(".accordion");

    accordionBtns.forEach(() => {
      var accordion =  (): void => {
        this.classList.toggle("is-open");
    
        let content = this.nextElementSibling;
        console.log(content);
    
        if (content.style.maxHeight) {
          //this is if the accordion is open
          content.style.maxHeight = null;
        } else {
          //if the accordion is currently closed
          content.style.maxHeight = content.scrollHeight + "px";
          console.log(content.style.maxHeight);
        }
      };
    });


  }

}
