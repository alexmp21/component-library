import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';


const routes: Routes = [
  // auth
  {path: 'auth', loadChildren:() => import('./auth/web/auth.module').then((m)=>m.AuthModule)},
  // view web
  // css-html
  {path: 'web', loadChildren:() => import('./view/web/CSS-HTML/csshtml.module').then((m)=>m.CsshtmlModule)},
  
  // redireccionamiento
  { path: '**',redirectTo: 'auth',pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash:true})],
  exports: [RouterModule]
})
export class appRoutingModule {}
