import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { RouterModule } from '@angular/router';
import { NgxSpinnerModule } from 'ngx-spinner';
import { InfoComponent } from './info/info.component';
import { CoreModule } from '../../core/core.module';
import { AuthRoutingModule } from './auth-routing.module';
import { SharedWebModule } from 'src/app/shared/web/sharedweb.module';

import { AuthestructuraComponent } from 'src/app/layouts/web/authestructura/authestructura.component';


@NgModule({
  declarations: [
    LoginComponent,
    InfoComponent,
    AuthestructuraComponent
  ],

  imports: [
    CommonModule,
     // svg-icon
     HttpClientModule, AngularSvgIconModule.forRoot(),
     // Modulo para que funcione el del router-outlet
     RouterModule,
     //  Ruta Propia
     AuthRoutingModule,
     // Spinner
     NgxSpinnerModule,
     //  Modulo
     CoreModule,
     SharedWebModule,
    

  ],
  exports: [
    LoginComponent,
    InfoComponent
  ]
})
export class AuthModule { }
