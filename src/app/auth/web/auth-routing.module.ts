import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { InfoComponent } from './info/info.component';
import { AuthestructuraComponent } from 'src/app/layouts/web/authestructura/authestructura.component';


const routes: Routes = [

  { path: '', component:  AuthestructuraComponent,
    children:[
      { path: 'inicio', component:LoginComponent },
      { path: 'info', component: InfoComponent },
      { path:'', pathMatch:'full', redirectTo:'inicio'}
    ]  
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {}
