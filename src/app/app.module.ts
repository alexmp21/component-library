import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
// iconos
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
// loading
import { NgxSpinnerModule } from "ngx-spinner";
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DemoMaterialModule } from './material-module';
import { RouterModule } from '@angular/router';
import { appRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    // svg-icon
    HttpClientModule,
    AngularSvgIconModule.forRoot(),
    // Modulo para que funcione el del router-outlet
    RouterModule,
    // Ruta General
    appRoutingModule,
    // Modulo
    NgxSpinnerModule,
    // Angular Material
    DemoMaterialModule,
   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
