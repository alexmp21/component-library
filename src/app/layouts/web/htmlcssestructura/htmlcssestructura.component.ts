import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-htmlcssestructura',
  templateUrl: './htmlcssestructura.component.html',
  styleUrls: ['./htmlcssestructura.component.css']
})
export class HtmlcssestructuraComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  sideBarOpen = true;

  sideBarToggler() {
    this.sideBarOpen = !this.sideBarOpen;
  }

}
